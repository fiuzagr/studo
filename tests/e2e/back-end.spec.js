describe('The back-end tasks view', function () {
  var page

  beforeEach(function () {
    browser.get('http://localhost:3000/#/back')
    page = require('./back-end.po')
  })

  it('should have the initial view the title defined on .env file', function() {
    expect(browser.getTitle()).toBe(process.env.APP_TITLE)
  })

  it('should have the initial view the url of back-end tasks', function() {
    expect(browser.getCurrentUrl()).toBe('http://localhost:3000/#/back-end/tasks/')
  })

  it('should have the breadcrumb the back-end text at last item', function() {
    expect(page.lastBreadcrumbItem.getText()).toBe('back-end')
  })

  it('should change url after click the front-end button', function() {
    page.frontEndButton.click()

    expect(browser.getCurrentUrl()).toBe('http://localhost:3000/#/front-end/tasks/')
  })
})
