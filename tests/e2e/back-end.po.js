/**
 * This file uses the Page Object pattern to define the main page for tests
 * https://docs.google.com/presentation/d/1B6manhG0zEXkC-H-tPo2vwU06JhL8w9-XCF9oehXzAQ
 */

function BackEndPage () {
  this.lastBreadcrumbItem = element(by.css('.breadcrumb .crumb:last-child'))
  this.frontEndButton = element(by.cssContainingText('.md-button', 'Front'))
}

module.exports = new BackEndPage()
