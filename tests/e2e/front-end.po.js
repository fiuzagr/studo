/**
 * This file uses the Page Object pattern to define the main page for tests
 * https://docs.google.com/presentation/d/1B6manhG0zEXkC-H-tPo2vwU06JhL8w9-XCF9oehXzAQ
 */

function FrontEndPage () {
  this.lastBreadcrumbItem = element(by.css('.breadcrumb .crumb:last-child'))
  this.backEndButton = element(by.cssContainingText('.md-button', 'Back'))
}

module.exports = new FrontEndPage()
