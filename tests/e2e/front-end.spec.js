describe('The front-end tasks view', function () {
  var page

  beforeEach(function () {
    browser.get('http://localhost:3000/')
    page = require('./front-end.po')
  })

  it('should have the initial view the title defined on .env file', function() {
    expect(browser.getTitle()).toBe(process.env.APP_TITLE)
  })

  it('should have the initial view the url of front-end tasks', function() {
    expect(browser.getCurrentUrl()).toBe('http://localhost:3000/#/front-end/tasks/')
  })

  it('should have the breadcrumb the front-end text at last item', function() {
    expect(page.lastBreadcrumbItem.getText()).toBe('front-end')
  })

  it('should change url after click the back-end button', function() {
    page.backEndButton.click()

    expect(browser.getCurrentUrl()).toBe('http://localhost:3000/#/back-end/tasks/')
  })
})
