<?php

header("Access-Control-Allow-Origin: *");

$mimeTypes = [
  '.css' => 'text/css',
  '.js'  => 'application/javascript',
  '.json' => 'application/json',
  '.jpg' => 'image/jpg',
  '.png' => 'image/png',
  '.svg' => 'image/svg+xml',
  '.map' => 'application/json'
];

$uri = $_SERVER['REQUEST_URI'];
$file = $_SERVER['SCRIPT_FILENAME'];


if (strpos($uri, "mock") !== false) {
  if (strpos($uri, ".json") !== false) {
    header("Content-Type: {$mimeTypes['.json']}");
    require $file;
    exit;
  } else {
    // Todo
  }
} else if (preg_match("/\.css|\.js(on)?|\.jpg|\.png|\.svg|\.map$/", $uri, $match)) {
  if (is_file($file)) {
    if (array_key_exists($match[0], $mimeTypes)) {
      header("Content-Type: {$mimeTypes[$match[0]]}");
    }
    require $file;
    exit;
  }
}

if (strpos($file, 'dev-server') === false) {
  require $file;
} else {
  die('Oops.');
}
