# Studo Todo App

> Todo app in Angular 1.5 with Angular Material, Babel (ES6 features),
> Redux, Webpack and Cordova.

* [Angular 1.5][angular-site]
* [Angular Material][angular-material-site]
* [Angular UI Router][angular-ui-router-site]
* [Redux][redux-site] + [NgRedux][ng-redux-site]
* [Webpack][webpack-site]
* [Babel][babel-site] (ES6 + ES7 decorators)
* [JSON-Server][json-server-site]
* [Cordova][cordova-site]
* [Protractor][protractor-site] + [Jasmine][jasmine-site]


---

## Requirements

* [Node.js][node-site] >= 6.5
* [Cordova][cordova-site]

*Optional*

* [Docker Toolbox][docker-toolbox-site]

---

## Quick Start

```shell
$ npm install
$ npm start
```

Or via docker-compose:

```shell
$ npm run docker -- <machine-name>
```

_Machine name is optional. The machine-name default is `default`_

---

## Development

```shell
$ npm install
```

After install node packages, the script that generates `.env` file
will be run automatically. If you need re-run env file generator script, execute:

```shell
$ npm run env
```

### Run servers

The below command run API server and DEV server.

```shell
$ npm start
```

If you need run separated API server or DEV server:

#### JSON-Server API:

```shell
$ npm run server:api
```

#### Webpack Dev Server:

```shell
$ npm run server:dev
```

---

## Build

All build process is base on `.env.production` file. Ensure that the file
is setted accordingly. If you want build to local tests, 
copy `.env` to `.env.production`. The `.env` files _never_ overrides predefined ENV vars.

Generate dist and Cordova packages:

```shell
$ npm run build
```

### Dist packages

Generate only dist packages with release and debug versions:

```shell
$ npm run build:dist
```

To build only release or debug:

#### Release:

```shell
$ npm run build:dist:release
```

#### Debug:

```shell
$ npm run build:dist:debug
```

### Cordova package

Generate only Cordova package:

```shell
$ npm run build:cordova
```


---

## Run build versions

_The API server should be run._

### Run Cordova

At first time, run:

```shell
$ npm run build:cordova
$ cd ./cordova
$ cordova platform add <platform>
$ cordova run <platform>
```

After first time, run only:

```shell
$ cd ./cordova
$ cordova run <platform>
```

_To run devices platform (android, ios), ensure that the device can
access the API server. And set `REST_BASE_URL` from `.env` file accordingly_

### Run PHP builtin server

Use to test release/debug dist packages.

#### Release:

```shell
$ php -S 0.0.0.0:8000 -t dist/release/ .dev-server.php
```

#### Debug:

```shell
$ php -S 0.0.0.0:8000 -t dist/debug/ .dev-server.php
```


---

## Tests

_The API server should be run._

```shell
$ npm run test
```

### E2E Protractor + Jasmine

_The Selenium server should be run._

```shell
$ npm run test:e2e
```


---

## Author

Fiuza, Guilherme R [@fiuzagr](http://fiuzagr.github.io)


[node-site]: https://nodejs.org/en/
[angular-site]: https://angularjs.org/
[angular-material-site]: https://material.angularjs.org/
[angular-ui-router-site]: https://ui-router.github.io/
[babel-site]: https://babeljs.io/
[redux-site]: http://redux.js.org/
[ng-redux-site]: https://github.com/angular-redux/ng-redux
[webpack-site]: https://webpack.github.io/
[cordova-site]: https://cordova.apache.org/
[json-server-site]: https://github.com/typicode/json-server
[jasmine-site]: http://jasmine.github.io/
[protractor-site]: http://www.protractortest.org/
[docker-toolbox-site]: https://www.docker.com/products/docker-toolbox
