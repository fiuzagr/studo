/**
 * Webpack Dev Configuration
 */

/** Define ENV constant, force development */
var ENV = process.env.NODE_ENV = process.env.ENV = 'development';

// import modules
var path = require('path');
var webpack= require('webpack');
var webpackMerge = require('webpack-merge');
var AppCachePlugin = require('appcache-webpack-plugin');

// import local modules
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

// adjust metadata to dev
var METADATA = commonConfig.metadata;

console.log(METADATA);

/**
 * Merge with Common Config and Export
 */
module.exports = webpackMerge(commonConfig, {

  // Generate source map file
  // devtool: 'eval-cheap-source-map',
  devtool: 'eval', // Fastter
  debug: true,
  // cache: true,

  // Output path and file name
  output: {
    path: helpers.root('dist', 'debug'),
    publicPath: commonConfig.metadata.publicPath,
    filename: 'js/[name].js',
    sourceMapFilename: 'js/[name].map',
    chunkFilename: '[id].chunk.js',
  },

  // configure webpack plugins
  plugins: [
    /**
     * Define Plugin
     *
     * Define free variables to global scope of JS files.
     *
     * See https://webpack.github.io/docs/list-of-plugins.html#defineplugin
     */
    new webpack.DefinePlugin({
      'process.env': {
        'ENV': JSON.stringify(ENV),
        'NODE_ENV': JSON.stringify(ENV),
        'APP_TITLE': JSON.stringify(commonConfig.metadata.title),
        'BUILD_VERSION': JSON.stringify(commonConfig.metadata.version),
        'REST_BASE_URL': JSON.stringify(process.env.REST_BASE_URL),
        'LOG_LEVEL': JSON.stringify(process.env.LOG_LEVEL || 5),
      },
    }),
    /**
     * App Cache Plugin
     *
     * Generate manifest file.
     *
     * See https://webpack.github.io/docs/list-of-plugins.html#defineplugin
     */
    new AppCachePlugin({
      output: '/manifest.appcache',
      settings: ['prefer-online'],
      exclude: [/.*\.*$/],
    }),
  ],

  /**
   * Webpack Dev Server Configuration
   *
   * The webpack-dev-server is a little node.js Express server.
   * The server emits information about the compilation state to the client,
   * which reacts to those events.
   *
   * See https://webpack.github.io/docs/webpack-dev-server.html
   */
  devServer: {
    contentBase: helpers.root('dist', 'debug'),
    port: commonConfig.metadata.port,
    host: commonConfig.metadata.host,
    historyApiFallback: true,
    watchOptions: {
      aggregateTimeout: 800,
      poll: 1000,
    },
  },

  /**
   * Node configuration
   *
   * Include polyfills or mocks for various node stuff.
   *
   * See https://webpack.github.io/docs/configuration.html#node
   */
  node: {
    global: 'window',
    crypto: 'empty',
    process: true,
    module: false,
    clearImmediate: false,
    setImmediate: false,
  },

});
