/**
 * Webpack Common Configuration
 */

/** Define ENV constant, force production */
var ENV = process.env.NODE_ENV = process.env.ENV = 'production';

// import modules
var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var AppCachePlugin = require('appcache-webpack-plugin');

// import local modules
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

// adjust metadata to cordova
var METADATA = commonConfig.metadata;
METADATA.cordova = true;
METADATA.publicPath = '';

console.log(METADATA);

/**
 * Merge with Common Config and Export
 */
module.exports = webpackMerge(commonConfig, {

  // Custom attribute
  metadata: METADATA,

  // Generate source map file
  devtool: 'source-map',

  // Output path and file name
  output: {
    path: helpers.root('cordova', 'www'),
    publicPath: METADATA.publicPath,
    filename: 'js/[name].[hash].js',
    chunkFilename: '[id].[hash].chunk.js',
  },

  // configure webpack plugins
  plugins: [
    /**
     * No Errors Plugin
     *
     * Does not generate output if some error occurred.
     *
     * See https://github.com/webpack/docs/wiki/list-of-plugins#noerrorsplugin
     */
    new webpack.NoErrorsPlugin(),
    /**
     * Dedupe Plugin
     *
     * Prevents the inclusion of duplicate code into your bundle
     * and instead applies a copy of the function at runtime.
     *
     * See https://github.com/webpack/docs/wiki/optimization#deduplication
     */
    new webpack.optimize.DedupePlugin(),
    /**
     * UglifyJs Plugin
     *
     * Minimize all JavaScript output of chunks.
     * Loaders are switched into minimizing mode.
     *
     * See https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
     */
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: true,
      },
    }),
    /**
     * Define Plugin
     *
     * Define free variables to global scope of JS files.
     *
     * See https://webpack.github.io/docs/list-of-plugins.html#defineplugin
     */
    new webpack.DefinePlugin({
      'process.env': {
        'ENV': JSON.stringify(ENV),
        'NODE_ENV': JSON.stringify(ENV),
        'APP_TITLE': JSON.stringify(METADATA.title),
        'BUILD_VERSION': JSON.stringify(METADATA.version),
        'REST_BASE_URL': JSON.stringify(process.env.REST_BASE_URL),
        'LOG_LEVEL': JSON.stringify(process.env.LOG_LEVEL || 0),
        'CORDOVA': JSON.stringify(true),
      },
    }),
    /**
     * App Cache Plugin
     *
     * Generate manifest file.
     *
     * See https://webpack.github.io/docs/list-of-plugins.html#defineplugin
     */
    new AppCachePlugin({
      output: '/manifest.appcache',
      settings: ['prefer-online'],
      exclude: [/assets/,/.*\.map$/], // exclude all map files
    }),
  ],

  /**
   * Html loader advanced options
   *
   * See https://github.com/webpack/html-loader#advanced-options
   */
  htmlLoader: {
    minimize: true,
    removeAttributeQuotes: false,
    caseSensitive: true,
    customAttrSurround: [
      [/#/, /(?:)/],
      [/\*/, /(?:)/],
      [/\[?\(?/, /(?:)/]
    ],
    customAttrAssign: [/\)?\]?=/],
  },

  /**
   * Node configuration
   *
   * Include polyfills or mocks for various node stuff.
   *
   * See https://webpack.github.io/docs/configuration.html#node
   */
  node: {
    global: 'window',
    crypto: 'empty',
    process: false,
    module: false,
    clearImmediate: false,
    setImmediate: false,
  },
});
