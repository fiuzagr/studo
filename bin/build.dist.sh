#!/bin/sh

BASE_DIR="$(cd "$(dirname $0)/.." && pwd)"
LOG_DIR="$BASE_DIR/logs"
LOG_FILE="$LOG_DIR/build.dist.$(date +%Y%m%d-%H%M%S).log"

mkdir -p $LOG_DIR 2> /dev/null

{
  exec 4>&1

  # Get exit status from first command
  EXIT_STATUS=`{
    {
      npm run build:dist 2>&1 3>&-;
      printf $? 1>&3
    } 4>&- | tee -a $LOG_FILE 1>&4
  } 3>&1`

  exec 4>&-

  if [ "$EXIT_STATUS" -ne "0" ]; then
    false
  fi
} || {
  printf "\n-------------------------------------------------\n"
  printf "\nOops. Something is not good! :("
  printf "\nSee the log: $LOG_FILE\n"
  printf "\n-------------------------------------------------\n"

  exit 0
}

printf "\n-------------------------------------------------\n"
printf "\nAll right! :)"
printf "\nSee the log: $LOG_FILE\n"
printf "\n-------------------------------------------------\n"

exit 0
