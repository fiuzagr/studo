#!/bin/sh

{
  BASE_DIR="$(cd "$(dirname $0)/.." && pwd)"
  export LOCAL_DOCKER_DIR="$BASE_DIR/docker"
  export LOCAL_DOCKER_COMPOSE_FILE="$LOCAL_DOCKER_DIR/docker-compose.yml"

  # default machine name
  DOCKER_MACHINE_NAME="default"

  # with 1 or more parameters
  if [ $# -ge 1 ]; then
    # get the first parameter
    DOCKER_MACHINE_NAME=$@
  fi

  # start machine
  docker-machine start $DOCKER_MACHINE_NAME

  # get IP from docker-machine
  export DOCKER_MACHINE_IP=$(docker-machine ip $DOCKER_MACHINE_NAME)

  # exit if not found ip
  if [ -z $DOCKER_MACHINE_IP ]; then
    exit 1
  fi

  # 
  # run docker
  # 
  # set env variables
  eval $(docker-machine env $DOCKER_MACHINE_NAME)
  # up compose
  docker-compose -f $LOCAL_DOCKER_COMPOSE_FILE up -d --force-recreate
  # open in browser
  open "http://$DOCKER_MACHINE_IP:3000"
}
