/**
 * vendor.js
 *
 * All modules vendor that need import.
 */

// Angular
import 'angular'
// Angular Aria
import 'angular-aria'
// Angular Animate
import 'angular-animate'
// Angular Material
import 'angular-material'
// Angular UI Router
import 'angular-ui-router'

// Redux
import 'redux'
import 'ng-redux'
