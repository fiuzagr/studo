/**
 * main.js
 *
 * Bootstrap application.
 */
//
// Inclue general styles
// This styles will be extracted by "Extract Text Plugin"
// Keep on top
import 'styles/index.styl'

//
// Import vendor
import * as angular from 'angular'

// Local reducers
import { reducers } from 'app/redux/reducers'

//
// Import local components
import {
  AppComponent,
  apiReduxMiddleware,
  isCordova
} from 'app'

/**
 * Configure main module
 */
AppComponent.module
  .factory('apiReduxMiddleware', apiReduxMiddleware)
  .config([
    '$urlRouterProvider',
    '$ngReduxProvider',
    ($urlRouterProvider, $ngReduxProvider) => {
      // url not found
      $urlRouterProvider.otherwise(($injector, $location) => {
        let path = $location.path()
        let taskType = path.indexOf('back') > -1 ? 'back-end' : 'front-end'
        $location.replace().path(`/${taskType}/tasks/`).search('completed&opened')
      })

      // Create store
      $ngReduxProvider.createStoreWith(reducers, ['apiReduxMiddleware'])
    }
  ])

function main () {
  angular.bootstrap(document, [AppComponent.module.name], {strictDi: true})
}

// Bootstrap
if (isCordova) {
  document.addEventListener('deviceready', main, false)
} else {
  angular
    .element(document)
    .ready(main)
}
