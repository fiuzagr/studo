/**
 * tasks.container.js
 *
 * Declare TasksContainer.
 */

// Import modules
import * as angular from 'angular'
import uiRouter from 'angular-ui-router'

// Import local components
import {
  Component,
  TaskListComponent,
  TaskFormComponent
} from 'app'

// Actions
import * as TasksActions from 'app/redux/tasks/tasks.actions'

// Import styles
import './tasks.styl'

// TasksContainer
@Component({
  moduleName: 'studo.tasks',
  directives: [
    TaskListComponent.moduleName,
    TaskFormComponent.moduleName
  ],
  providers: [
    uiRouter
  ],
  selector: 'studo-tasks',
  template: require('./tasks.html'),
  route: {
    url: '/{taskType}/tasks/{taskId}?completed&opened',
    views: {
      'app@studo': {
        template: '<studo-tasks></studo-tasks>'
      }
    }
  }
})
export class TasksContainer {
  static $inject = ['$ngRedux', '$stateParams', '$mdDialog']

  constructor ($ngRedux, $stateParams, $mdDialog) {
    this.$ngRedux = $ngRedux
    this.$stateParams = $stateParams
    this.$mdDialog = $mdDialog

    this.connectRedux()
  }

  connectRedux () {
    let completed = this.$stateParams.completed === 'true'
    let opened = this.$stateParams.opened === 'true'

    this.disconnectRedux = this.$ngRedux.connect(state => ({
      tasks: state.tasks
    }), TasksActions)((state, actions) => {
      if (!state.tasks.isLoaded) actions.fetchTasks()

      this.actions = actions

      this.taskList = state.tasks.list.filter((item) => {
        let type = item.type === this.$stateParams.taskType
        let filter = false

        filter = (opened && !item.completed) ||
          (completed && item.completed)

        return type && filter
      })
    })
  }

  toggleTaskForm () {
    if (this.$stateParams.taskId) {
      this.$mdDialog.show({
        contentElement: '#taskForm',
        parent: angular.element(document.body)
      })
    } else {
      this.$mdDialog.hide()
    }
  }

  $onInit () {
    this.toggleTaskForm()
  }

  $onChanges () {
    this.toggleTaskForm()
  }

  $onDestroy () {
    this.disconnectRedux()
  }

}

