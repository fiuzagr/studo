/**
 * api-redux.middleware.js
 *
 * API Middleware for redux.
 */

export function apiReduxMiddleware ($http) {
  return store => next => action => { // eslint-disable-line no-unused-vars
    if (!action.api) return next(action)

    const { meta } = action
    const { config, types } = action.api
    const [requestType, successType, errorType] = types

    next({type: requestType})

    $http(config)
      .then(response => {
        next({
          type: successType,
          payload: response.data,
          meta
        })
      })
      .catch(response => {
        next({
          type: errorType,
          error: true,
          payload: response.data,
          meta
        })
      })
  }
}

apiReduxMiddleware.$inject = ['$http']
