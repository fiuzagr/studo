/**
 * app.settings.js
 *
 * Application settings.
 */

/**
 * isProduction
 */
export const isProduction = (
  process.env.ENV === 'production'
)

/**
 * isCordova
 */
export const isCordova = (
  process.env.CORDOVA === 'true'
)

/**
 * debugMode
 */
export const debugMode = !(
  isProduction
)

/**
 * buildVersion
 */
export const buildVersion = (
  process.env.BUILD_VERSION
)

/**
 * restBaseUrl
 */
export const restBaseUrl = (
  process.env.REST_BASE_URL
)

/**
 * appTitle
 */
export const appTitle = (
  process.env.APP_TITLE
)

/**
 * logLevel
 */
export const logLevel = (
  process.env.LOG_LEVEL
)

