/**
 * component.decorator.js
 *
 * Component ES7 decorator
 */

//
// Import modules
import * as angular from 'angular'

// Component
export function Component (settings) {
  let {
    moduleName,
    selector,
    directives = [],
    providers = [],
    template = null,
    templateUrl = null,
    bindings = {},
    require = {},
    transclude = false,
    route = null
  } = settings

  // define providers
  let moduleProviders = []
  moduleProviders.push(...directives)
  moduleProviders.push(...providers)

  // adjust selector
  selector = selector
    .replace(/[-\s]+\b\w/g, (letter) => {
      return letter.toUpperCase().replace(/[-\s]+/g, '')
    })

  return (target) => {
    target.moduleName = moduleName

    target.module = angular
      .module(moduleName, moduleProviders)
      .component(selector, {
        controller: target,
        template,
        templateUrl,
        bindings,
        require,
        transclude
      })

    if (route) {
      target.module.config([
        '$stateProvider',
        ($stateProvider) => {
          $stateProvider
            .state(moduleName, route)
        }
      ])
    }
  }
}
