/**
 * nav-toolbar.component.js
 *
 * Declare Nav Toolbar Component.
 */

// Import local components
import { Component } from 'app'

// Import styles
import './nav-toolbar.styl'

// FrontEndComponent
@Component({
  moduleName: 'studo.component.navToolbar',
  selector: 'studo-nav-toolbar',
  template: require('./nav-toolbar.html')
})
export class NavToolbarComponent {
  static $inject = ['$state']

  constructor ($state) {
    this.$state = $state
    this.currentNavItem = $state.params.taskType
  }

  newTask () {
    let taskType = this.$state.params.taskType
    let taskId = 'new'
    this.$state.go('studo.tasks', {taskType, taskId})
  }
}

