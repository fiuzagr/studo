/**
 * task-list.component.js
 *
 * Declare TaskListComponent.
 */

// Import local components
import {
  Component
} from 'app'

import {
  TaskItemComponent
} from 'app/components'

// Import styles
import './task-list.styl'

// TaskListComponent
@Component({
  moduleName: 'studo.component.task-list',
  directives: [
    TaskItemComponent.moduleName
  ],
  selector: 'studo-task-list',
  template: require('./task-list.html'),
  bindings: {
    list: '<',
    onUpdate: '&',
    onDelete: '&'
  }
})
export class TaskListComponent {
  update (task) {
    return this.onUpdate({task: task})
  }

  delete (task) {
    return this.onDelete({task: task})
  }
}

