/**
 * task-item.component.js
 *
 * Declare TaskItemComponent.
 */

// Import local components
import { Component } from 'app'

// Import styles
import './task-item.styl'

// TaskItemComponent
@Component({
  moduleName: 'studo.component.task-item',
  selector: 'studo-task-item',
  template: require('./task-item.html'),
  bindings: {
    task: '<',
    onUpdate: '&',
    onDelete: '&'
  }
})
export class TaskItemComponent {
  complete () {
    this.task.completed = !this.task.completed
    return this.update()
  }

  update () {
    return this.onUpdate({task: this.task})
  }

  delete () {
    return this.onDelete({task: this.task})
  }
}

