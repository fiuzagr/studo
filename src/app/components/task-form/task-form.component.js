/**
 * task-form.component.js
 *
 * Declare TaskFormComponent.
 */

// Import local components
import { Component } from 'app'

// Import styles
import './task-form.styl'

// TaskFormComponent
@Component({
  moduleName: 'studo.component.task-form',
  selector: 'studo-task-form',
  template: require('./task-form.html'),
  bindings: {
    onSave: '&'
  }
})
export class TaskFormComponent {
  static $inject = ['$state']

  constructor ($state) {
    this.$state = $state

    this.taskType = this.$state.params.taskType || 'front-end'

    this.title = ''
    this.type = this.taskType
  }

  save () {
    this.close()
    return this.onSave({title: this.title, type: this.type})
  }

  close () {
    let taskType = this.taskType
    let taskId = null

    this.$state.go('studo.tasks', {taskType, taskId})
  }
}

