/**
 * breadcrumb.component.js
 *
 * Declare Breadcrumb Component.
 */

// Import local components
import { Component } from 'app'

// Import styles
import './breadcrumb.styl'

// BreadcrumbComponent
@Component({
  moduleName: 'studo.component.breadcrumb',
  selector: 'studo-breadcrumb',
  template: require('./breadcrumb.html'),
  bindings: {
    list: '<'
  }
})
export class BreadcrumbComponent {
}

