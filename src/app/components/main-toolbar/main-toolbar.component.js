/**
 * main-toolbar.component.js
 *
 * Declare Main Toolbar Component.
 */

// Import local components
import { Component } from 'app'
import { BreadcrumbComponent } from 'app/components'

// Import styles
import './main-toolbar.styl'

// FrontEndComponent
@Component({
  moduleName: 'studo.component.mainToolbar',
  directives: [
    BreadcrumbComponent.moduleName
  ],
  selector: 'studo-main-toolbar',
  template: require('./main-toolbar.html'),
  bindings: {
    crumbList: '<'
  }
})
export class MainToolbarComponent {
  static $inject = ['$state', '$stateParams']

  constructor ($state, $stateParams) {
    this.$state = $state
    this.$stateParams = $stateParams

    this.filters = {
      completed: $stateParams.completed === 'true',
      opened: $stateParams.opened === 'true'
    }
  }

  toggleFilter (filter) {
    let params = this.$stateParams

    this.filters[filter] =
      params[filter] = params[filter] !== 'true'

    this.$state.go('studo.tasks', params)
  }
}

