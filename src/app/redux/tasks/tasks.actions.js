import { restBaseUrl } from 'app/shared/app.settings'

import {
  CREATE_TASK_TYPES,
  DELETE_TASK_TYPES,
  FETCH_TASKS_TYPES,
  UPDATE_TASK_TYPES
} from './tasks.action-types'

export function createTask (data) {
  return {
    api: {
      types: CREATE_TASK_TYPES,
      config: {
        data: {completed: false, ...data},
        method: 'post',
        url: `${restBaseUrl}/tasks`
      }
    }
  }
}

export function deleteTask (task) {
  return {
    api: {
      types: DELETE_TASK_TYPES,
      config: {
        method: 'delete',
        url: `${restBaseUrl}/tasks/${task.id}`
      }
    },
    meta: {
      id: task.id
    }
  }
}

export function fetchTasks () {
  return {
    api: {
      types: FETCH_TASKS_TYPES,
      config: {
        method: 'get',
        url: `${restBaseUrl}/tasks`
      }
    }
  }
}

export function updateTask (task) {
  return {
    api: {
      types: UPDATE_TASK_TYPES,
      config: {
        data: task,
        method: 'put',
        url: `${restBaseUrl}/tasks/${task.id}`
      }
    }
  }
}
