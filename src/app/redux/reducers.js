/**
 * reducers.js
 *
 * Export combined reducers.
 */

//
// Import modules
import { combineReducers } from 'redux'

//
// Import reducers
import {
  TasksReducer as tasks
} from 'app'

// reducers
export const reducers = combineReducers({
  tasks
})
