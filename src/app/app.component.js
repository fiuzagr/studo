/**
 * app.component.js
 *
 * Declare Application Component.
 */

//
// Import vendor dependencies
import ngMaterial from 'angular-material'
import uiRouter from 'angular-ui-router'
import ngRedux from 'ng-redux'

// Import local components
import {
  Component,
  TasksContainer,
  MainToolbarComponent,
  NavToolbarComponent
} from 'app'

// Import styles
import './app.styl'

// AppComponent
@Component({
  moduleName: 'studo',
  directives: [
    TasksContainer.moduleName,
    MainToolbarComponent.moduleName,
    NavToolbarComponent.moduleName
  ],
  providers: [
    ngMaterial,
    uiRouter,
    ngRedux
  ],
  selector: 'studo-app',
  template: require('./app.html'),
  route: {
    url: '',
    abstract: true,
    template: '<studo-app></studo-app>'
  }
})
export class AppComponent {
  static $inject = ['$rootScope', '$state']

  constructor ($rootScope, $state) {
    this.updateBreadcrumb($state.params.taskType)

    $rootScope.$on('$stateChangeSuccess',
      (event, toState, toParams) => {
        this.updateBreadcrumb(toParams.taskType)
      }
    )
  }

  updateBreadcrumb (crumb) {
    this.breadcrumb = ['todos', crumb]
  }
}

